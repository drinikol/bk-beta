﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MEC;
public class LayoutBoard : MonoBehaviour {
    public const int gridRows = 2;
    public const int gridCols = 4;
    public const float offsetX = 3.5f;
    public const float offsetY = 4.7f;
    AudioSources inst;
    CameraVibrateUI vibrateMe;
    [SerializeField] private CardFlip originalCard;
    [SerializeField] private List<Sprite> images = new List<Sprite>();

    private CardFlip firstClicked;
    private CardFlip secondClicked;

    // Use this for initialization
    void Start () {
        Vector2 startPos = originalCard.transform.position; // starting point of offSet
        inst = AudioSources.instance;
        vibrateMe = GameObject.FindGameObjectWithTag("UICanvas").GetComponent<CameraVibrateUI>();

        List<int> numbers = new List<int>(); // populate the numbers
        for(int i = 0; i < 4; i++)
        {
            numbers.Add(i);
            numbers.Add(i);
        }

        numbers = ShuffleTheList(numbers);
        for (int columns = 0; columns < gridCols; columns++)
        {
            for(int rows = 0; rows < gridRows; rows++)
            {
                CardFlip card;
                if(columns == 0 && rows == 0)
                {
                    card = originalCard;
                } else
                {
                    card = Instantiate(originalCard) as CardFlip;
                    
                }
                int index = rows * gridCols + columns;
                int id = numbers[index];

                card.ChangeSprite(id, images[id]);

                float posX = (offsetX * columns) + startPos.x;
                float posY = (offsetY * rows) + startPos.y;
                card.transform.position = new Vector2(posX,posY);
                card.transform.SetParent(GameObject.Find("UICanvas").transform); //Place inside UI Canvas
                card.transform.localScale = new Vector3(originalCard.transform.localScale.x, originalCard.transform.localScale.y, transform.localScale.z);//Set Scale
            }
        }
     
    }
	
    private List<int> ShuffleTheList(List<int> theList)
    {
        Shuffler shuffling = new Shuffler();
        shuffling.Shuffle(theList);

        return theList;
    }

    public bool canShow
    {
        get { return secondClicked == null; }
    }

    public void CardShowed(CardFlip card)
    {
        if(firstClicked == null)
        {
            firstClicked = card;
        } else
        {
            secondClicked = card;
            Timing.RunCoroutine(_CheckCards());
        }
    }
    IEnumerator<float> _CheckCards()
    {
        if(firstClicked.GetId() == secondClicked.GetId()) {
            inst.PlayCorrectSFX();
            secondClicked.ShowCard();

        } else
        {
            yield return Timing.WaitForSeconds(0.5f);
            inst.PlayErrorSFX();
            vibrateMe.VibrateForTime(0.2f);
            firstClicked.HideCard();
            secondClicked.HideCard();
            
        }
        firstClicked = null;
        secondClicked = null;
    }
}
