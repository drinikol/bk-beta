﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor (typeof(ShapesManager))]
public class ShapesManagerEditor : Editor
{
	//private static bool showInstructions = false;

	public override void OnInspectorGUI ()
	{
		ShapesManager shapesManager = (ShapesManager)target;//get the target

		shapesManager.shapeLabel = EditorGUILayout.TextField ("Shape Label", shapesManager.shapeLabel);
		shapesManager.shapePrefix = EditorGUILayout.TextField ("Shape Prefix", shapesManager.shapePrefix);
		shapesManager.sceneName = EditorGUILayout.TextField ("Scene Name", shapesManager.sceneName);
		shapesManager.albumTitle = (Sprite)EditorGUILayout.ObjectField ("Album Title", shapesManager.albumTitle, typeof(Sprite));
		shapesManager.albumShapeScaleFactor = EditorGUILayout.Slider ("Album Shape Scale Factor", shapesManager.albumShapeScaleFactor, 0.1f, 10);
		//shapesManager.gameShapeScaleFactor = EditorGUILayout.Slider ("Game Shape Scale Factor", shapesManager.gameShapeScaleFactor, 0.1f, 10);

		EditorGUILayout.Separator ();

		GUILayout.BeginHorizontal ();
		GUI.backgroundColor = Colors.greenColor;         

		if (GUILayout.Button ("Add New Shape", GUILayout.Width (110), GUILayout.Height (20))) {
			if (shapesManager.shapePrefix.ToLower () == "sentence") {
				shapesManager.shapes.Add (new ShapesManager.Shape (){ starsTimePeriod = 25});
			} else {
				shapesManager.shapes.Add (new ShapesManager.Shape (){ starsTimePeriod = 15});
			}
		}

		GUI.backgroundColor = Colors.redColor;         
		if (GUILayout.Button ("Remove Last Shape", GUILayout.Width (150), GUILayout.Height (20))) {
			if (shapesManager.shapes.Count != 0) {
				shapesManager.shapes.RemoveAt (shapesManager.shapes.Count - 1);
			}
		}

		GUI.backgroundColor = Colors.whiteColor;
		GUILayout.EndHorizontal ();

		EditorGUILayout.Separator ();

		for (int i = 0; i < shapesManager.shapes.Count; i++) {
			shapesManager.shapes [i].showContents = EditorGUILayout.Foldout (shapesManager.shapes [i].showContents, "Shape[" + i + "]");

			if (shapesManager.shapes [i].showContents) {
				EditorGUILayout.Separator ();
				shapesManager.shapes [i].picture = EditorGUILayout.ObjectField ("Picture", shapesManager.shapes [i].picture, typeof(Sprite), true) as Sprite;
				shapesManager.shapes [i].gamePrefab = EditorGUILayout.ObjectField ("Game Prefab", shapesManager.shapes [i].gamePrefab, typeof(GameObject), true) as GameObject;
				shapesManager.shapes [i].clip = EditorGUILayout.ObjectField ("Clip", shapesManager.shapes [i].clip, typeof(AudioClip), true) as AudioClip;
				shapesManager.shapes [i].starsTimePeriod = EditorGUILayout.IntSlider ("Stars Time Period", shapesManager.shapes [i].starsTimePeriod, 5, 500);

				EditorGUILayout.Separator ();
				GUILayout.Box ("", GUILayout.ExpandWidth (true), GUILayout.Height (2));
			}
		}

		if (GUI.changed) {
			DirtyUtil.MarkSceneDirty ();
		}
	}
}