﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEditor.SceneManagement;

[CustomEditor (typeof(CompoundShape))]
public class CompoundShapeEditor : Editor
{
	private static string previousText;
	private static Vector2 previousCharacterOffset, previousSentenceOffset;
	private static float previousSentencesScaleFactor;
	private static bool showShapes = true;

	public override void OnInspectorGUI ()
	{
		CompoundShape compoundShape = (CompoundShape)target;//get the target

		if (PrefabUtility.GetPrefabType (compoundShape.gameObject) == PrefabType.Prefab) {
			EditorGUILayout.HelpBox ("You are allowed to edit the attributes in the Hierarchy layout only. Drag and drop the prefab to the Hierarchy under Shape gameobject and edit it", MessageType.Warning);
			return;
		}

		SerializedObject attrib = new SerializedObject (target);
		attrib.Update ();

		EditorGUILayout.Separator ();

		EditorGUILayout.PropertyField (attrib.FindProperty ("text"), true);

		EditorGUILayout.Separator ();
		EditorGUILayout.PropertyField (attrib.FindProperty ("characterOffset"), true);
		EditorGUILayout.PropertyField (attrib.FindProperty ("sentenceScaleFactor"), true);
		EditorGUILayout.PropertyField (attrib.FindProperty ("sentenceOffset"), true);

		EditorGUILayout.BeginVertical ();
		EditorGUILayout.HelpBox ("Click on the Create button to save the sentence in the Prefabs folder", MessageType.Info);
		EditorGUILayout.HelpBox ("Click on the top Apply button to apply changes on the sentence's prefab", MessageType.Info);

		string btnName = "Create Prefab";

		GameObject loadedObject = null;
		if (compoundShape != null)
			loadedObject = AssetDatabase.LoadAssetAtPath ("Assets/Bahay Kubo/Prefabs/Sentences/" + compoundShape.text.Trim ().Replace ("\n", "") + "-sentence.prefab", typeof(GameObject)) as GameObject;

		if (loadedObject != null)
		if (loadedObject.GetComponent<CompoundShape> ().text.Trim () == compoundShape.text.Trim ()) {
			EditorGUI.BeginDisabledGroup (true);
			btnName = "Prefab Already Exists";
		}

		if (previousText != compoundShape.text || Vector2.Distance(previousCharacterOffset,compoundShape.characterOffset) !=0 ||
			Vector2.Distance(previousSentenceOffset,compoundShape.sentenceOffset) !=0 || previousSentencesScaleFactor!=compoundShape.sentenceScaleFactor
		) {
			previousText = compoundShape.text;
			previousCharacterOffset = compoundShape.characterOffset;
			previousSentenceOffset = compoundShape.sentenceOffset;
			previousSentencesScaleFactor = compoundShape.sentenceScaleFactor;
			compoundShape.Generate ();
		}

		EditorGUILayout.BeginHorizontal ();

		GUI.backgroundColor = Colors.whiteColor;

		if (compoundShape.shapes.Count != 0) {
			GUI.backgroundColor = Colors.greenColor;    

			if (btnName != "Create Prefab") {
				GUI.backgroundColor = Colors.redColor;    
			}
			if (GUILayout.Button (btnName, GUILayout.Width (150), GUILayout.Height (20))) {
				compoundShape.Generate ();
				CreateNew (compoundShape.gameObject);
			}
		}

		EditorGUI.EndDisabledGroup ();
		GUI.backgroundColor = Colors.greenColor;    

		if (GUILayout.Button ("RE-Generate", GUILayout.Width (120), GUILayout.Height (20))) {
			compoundShape.Generate ();
		}

		EditorGUILayout.EndHorizontal ();
		EditorGUILayout.EndVertical ();
		EditorGUILayout.Separator ();

		attrib.ApplyModifiedProperties ();

		GUI.backgroundColor = Colors.whiteColor;    
		showShapes = EditorGUILayout.Foldout (showShapes, "Shapes");
		if (showShapes) {
			for (int i = 0; i < compoundShape.shapes.Count; i++) {
				compoundShape.shapes [i] = EditorGUILayout.ObjectField ("Element " + i, compoundShape.shapes [i], typeof(Shape), true) as Shape;
			}
		}

		if (GUI.changed) {
			DirtyUtil.MarkSceneDirty ();
		}
	}

	private void CreateNew (GameObject gameObject)
	{
		if (gameObject == null) {
			return;
		}

		//Set the path as within the Assets folder, and name it as the GameObject's name with the .prefab format
		string localPath = "Assets/Bahay Kubo/Prefabs/Sentences/" + gameObject.name + ".prefab";

		Object prefab = PrefabUtility.CreatePrefab (localPath, gameObject);
		PrefabUtility.ReplacePrefab (gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
		Debug.Log (gameObject.name + " is created sucessfully");
		Selection.activeObject = AssetDatabase.LoadMainAssetAtPath (localPath);
	}
}	