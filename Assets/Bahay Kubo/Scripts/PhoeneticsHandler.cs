﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using MEC;

[RequireComponent(typeof(AudioSource))]
public class PhoeneticsHandler : MonoBehaviour
{
    //Handles Scriptable Object of Phonetics
    [SerializeField] ObjectHolder[] phonetixObjects;

    [SerializeField] ObjectHolder[] phonetixNumbers;

    [SerializeField] GameObject phonetixTrace;
    [SerializeField] GameObject phonetixSprite;
    [SerializeField] GameObject phonetixLetter;
    [SerializeField] GameObject phonetixTagalogWord;
    [SerializeField] GameObject phonetixEnglishWord;
    [SerializeField] GameObject phonetixCanvas;
    [SerializeField] GameObject lettersHolder;
    [SerializeField] GameObject numbersHolder;
    [SerializeField] AudioSource audioSows;


    Image tixSprite;
    TextMeshProUGUI tixLetter;
    TextMeshProUGUI tixTWord;
    TextMeshProUGUI tixEWord;
    Button tixButton;
    void Awake()
    {
        phonetixCanvas.SetActive(false);
        if (UIEvents.isWhatShape == "numbers")
        {
            lettersHolder.SetActive(false);
            numbersHolder.SetActive(true);
        }
        else if (UIEvents.isWhatShape == "letters")
        {
            lettersHolder.SetActive(true);
            numbersHolder.SetActive(false);
        }
    }

    void Start()
    {

        // AudioSource audioSows = this.gameObject.AddComponent(typeof(AudioSource)) as AudioSource;
        tixSprite = phonetixSprite.GetComponent<Image>();
        tixLetter = phonetixLetter.GetComponent<TextMeshProUGUI>();
        tixTWord = phonetixTagalogWord.GetComponent<TextMeshProUGUI>();
        tixEWord = phonetixEnglishWord.GetComponent<TextMeshProUGUI>();

        tixButton = phonetixTrace.GetComponent<Button>();
        tixButton.interactable = false;
        phonetixCanvas.SetActive(false);
        tixSprite.color = new Color(tixSprite.color.r, tixSprite.color.g, tixSprite.color.b, 0f);


    }

    // Update is called once per frame

    //IEnumerator<float> ReplacePlaceHolders(int indexOfLetter)
    //{
    //    //yield return new WaitForSeconds(2f);
    //    yield return Timing.WaitForSeconds(2f);
    //    tixSprite.sprite = phonetixObjects[indexOfLetter].GetShapeWordSprite();

    //    tixLetter.text = phonetixObjects[indexOfLetter].shapeLetter;
    //    tixTWord.text = phonetixObjects[indexOfLetter].shapeWordTagalog;
    //    tixEWord.text = phonetixObjects[indexOfLetter].shapeWordEnglish;

    //    //StartCoroutine(PlaySound(indexOfLetter));
    //    Timing.RunCoroutine(PlaySound(indexOfLetter));


    //}

    public IEnumerator<float> StartPhonetix(int indexOfLetter)
    {
        if (UIEvents.isWhatShape == "numbers")
        {

            numbersHolder.SetActive(false);
        }
        else if (UIEvents.isWhatShape == "letters")
        {
            lettersHolder.SetActive(false);
        }
        //AudioSource audio = this.gameObject.GetComponent<AudioSource>();
        phonetixCanvas.SetActive(true);
        tixButton.interactable = false;
        ClearImage();

        yield return Timing.WaitForSeconds(0.25f);
        tixLetter.text = phonetixObjects[indexOfLetter].shapeLetter;

        audioSows.clip = phonetixObjects[indexOfLetter].GetEnglishSound();
        audioSows.Play();

        yield return Timing.WaitForSeconds(audioSows.clip.length);

        audioSows.clip = phonetixObjects[indexOfLetter].GetTagalogSound();
        audioSows.Play();
        //yield return new WaitForSeconds(audio.clip.length);
        yield return Timing.WaitForSeconds(audioSows.clip.length + 0.5f);
        audioSows.clip = phonetixObjects[indexOfLetter].GetShapeWordSound();
        audioSows.Play();
        tixSprite.sprite = phonetixObjects[indexOfLetter].GetShapeWordSprite();
        tixSprite.color = new Color(tixSprite.color.r, tixSprite.color.g, tixSprite.color.b, 1f);
        yield return Timing.WaitForSeconds(0.5f);
        tixTWord.text = phonetixObjects[indexOfLetter].shapeWordTagalog;
        tixEWord.text = phonetixObjects[indexOfLetter].shapeWordEnglish;
        tixButton.interactable = true;

    }

    public IEnumerator<float> StartPhonetixNumbers(int indexOfLetter)
    {
        if (UIEvents.isWhatShape == "numbers")
        {

            numbersHolder.SetActive(false);
        }
        else if (UIEvents.isWhatShape == "letters")
        {
            lettersHolder.SetActive(false);
        }
        //AudioSource audio = this.gameObject.GetComponent<AudioSource>();
        phonetixCanvas.SetActive(true);
        tixButton.interactable = false;
        ClearImage();

        yield return Timing.WaitForSeconds(0.25f);
        tixLetter.text = phonetixNumbers[indexOfLetter].shapeLetter;

        audioSows.clip = phonetixNumbers[indexOfLetter].GetEnglishSound();
        audioSows.Play();

        yield return Timing.WaitForSeconds(audioSows.clip.length);

        audioSows.clip = phonetixNumbers[indexOfLetter].GetTagalogSound();
        audioSows.Play();
        //yield return new WaitForSeconds(audio.clip.length);
        yield return Timing.WaitForSeconds(audioSows.clip.length + 0.5f);
        audioSows.clip = phonetixNumbers[indexOfLetter].GetShapeWordSound();
        audioSows.Play();
        tixSprite.sprite = phonetixNumbers[indexOfLetter].GetShapeWordSprite();
        tixSprite.color = new Color(tixSprite.color.r, tixSprite.color.g, tixSprite.color.b, 1f);
        yield return Timing.WaitForSeconds(0.5f);
        tixTWord.text = phonetixNumbers[indexOfLetter].shapeWordTagalog;
        tixEWord.text = phonetixNumbers[indexOfLetter].shapeWordEnglish;
        tixButton.interactable = true;

    }

    private void ClearImage()
    {
        tixSprite.color = new Color(tixSprite.color.r, tixSprite.color.g, tixSprite.color.b, 0f);
        tixSprite.sprite = null;
        tixLetter.text = null;
        tixTWord.text = null;
        tixEWord.text = null;
    }
}
