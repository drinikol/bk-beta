﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("BahayKubo/Shapes"))]
public class ObjectHolder : ScriptableObject{
    public string shapeLetter;
    public string shapeWordTagalog;
    public string shapeWordEnglish;
    [SerializeField] private AudioClip englishSound;
    [SerializeField] private AudioClip tagalogSound;
    [SerializeField] private AudioClip shapeWordSound;
    [SerializeField] private int elementId;
    [SerializeField] private Sprite shapeWordSprite;


    public int GetElementId()
    {
        return elementId;
    }

    public AudioClip GetEnglishSound()
    {
        return englishSound;
    }
    public AudioClip GetTagalogSound()
    {
        return tagalogSound;
    }
    public AudioClip GetShapeWordSound()
    {
        return shapeWordSound;
    }
    public Sprite GetShapeWordSprite()
    {
        return shapeWordSprite;
    }

}
