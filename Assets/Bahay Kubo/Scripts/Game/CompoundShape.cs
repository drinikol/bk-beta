﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine.SceneManagement;

[DisallowMultipleComponent]
public class CompoundShape : MonoBehaviour
{
	/// <summary>
	/// The shapes list.
	/// </summary>
	public List<Shape> shapes = new List<Shape> ();

	/// <summary>
	/// The text of the compound shape.
	/// </summary>
	[SerializeField]
	[TextArea (5, 5)]
	public string text ="";

	/// <summary>
	/// Whether to enable the shapes priority order or not.
	/// </summary>
	//public bool enablePriorityOrder = true;

	// Use this for initialization
	void Start ()
	{
	}


	/// <summary>
	/// Get the index of the current shape.
	/// </summary>
	/// <returns>The current shape index.</returns>
	public int GetCurrentShapeIndex ()
	{
		int index = -1;
		for (int i = 0; i < shapes.Count; i++) {

			if (shapes [i].completed) {
				continue;
			}

			bool isCurrentPath = true;
			for (int j = 0; j < i; j++) {
				if (!shapes [j].completed) {
					isCurrentPath = false;
					break;
				}
			}

			if (isCurrentPath) {
				index = i;
				break;
			}
		}

		return index;
	}

	/// <summary>
	/// Gets the shape's index by instance ID.
	/// </summary>
	/// <returns>The shape index by instance Id.</returns>
	/// <param name="ID">Instance ID.</param>
	public int GetShapeIndexByInstanceID (int ID)
	{
		for (int i = 0; i < shapes.Count; i++) {
			if (ID == shapes [i].GetInstanceID ()) {
				return i;
			}
		}
		return -1;
	}

	/// <summary>
	/// Determine whether the compound shape is completed.
	/// </summary>
	/// <returns><c>true</c> if this instance is completed; otherwise, <c>false</c>.</returns>
	public bool IsCompleted ()
	{
		bool completed = true;
		foreach (Shape shape in shapes) {
			if (!shape.completed) {
				completed = false;
				break;
			}
		}
		return completed;
	}
		

	#if UNITY_EDITOR

	[MenuItem ("Tools/Bahay Kubo/Create/Sentence #s", false, 0)]
	static void CreateSentence ()
	{
		GameObject newSentece = new GameObject ("new-sentence");
		RectTransform rectTransform = newSentece.AddComponent<RectTransform> ();
		newSentece.AddComponent<CompoundShape> ();
		newSentece.transform.SetParent (GameObject.Find ("Shape").transform);
		newSentece.transform.localScale = Vector3.one;
		rectTransform.anchorMin = Vector3.zero;
		rectTransform.anchorMax = Vector3.one;
		rectTransform.offsetMax = rectTransform.offsetMin = Vector3.zero;
		newSentece.transform.localPosition = Vector3.zero;
		Selection.activeObject = newSentece;
	}

	[MenuItem ("Tools/Bahay Kubo/Create/Sentence #s", true, 0)]
	static bool CreateSentenceValidate ()
	{
		return !Application.isPlaying && SceneManager.GetActiveScene ().name == "Game";
	}

	public Vector2 characterOffset = new Vector2(6,8);
	[Range (0, 10)]
	public float sentenceScaleFactor = 0.4f;
	public Vector2 sentenceOffset = Vector2.zero;

	public void Generate ()
	{
		//build shapes mapping list for the sentences
		Dictionary<string,GameObject> shapesMapping = new Dictionary<string, GameObject> ();
		List<ShapesManager> shapesManagers = GameObject.FindObjectOfType<SingletonManager> ().GetShapesManagers ();

		string shapeName = "";
		foreach (ShapesManager sm in shapesManagers) {
			foreach (ShapesManager.Shape shape in sm.shapes) {
				shapeName = shape.gamePrefab.name.Split ('-') [0];
				if (!shapesMapping.ContainsKey (shapeName)) {
					shapesMapping.Add (shapeName, shape.gamePrefab);
				}
			}
		}

		//generate the sentence based on the text

		foreach (Shape shape in shapes) {
			if (shape != null)
				DestroyImmediate (shape.gameObject);
		}

		shapes.Clear ();

		List<Transform> children = new List<Transform> ();
		foreach (Transform child in transform) {
			children.Add (child);
		}
		foreach (Transform child in children) {
			DestroyImmediate (child.gameObject);
		}

		text = text.Trim ();

		gameObject.name= text.Replace('\n',' ')+"-sentence";

		string[] lines = text.Split ('\n');


		transform.localScale = Vector3.one * sentenceScaleFactor;
		transform.localPosition = new Vector3(sentenceOffset.x,sentenceOffset.y,0);

		Vector2 tempCharacterOffset = characterOffset * sentenceScaleFactor;

		float xPos, yPos, extraOffset;
		for (int i = 0; i < lines.Length; i++) {
			//new line
			yPos = transform.position.y + -i * tempCharacterOffset.y;
				
			for (int j = 0; j < lines [i].Length; j++) {

				if (lines [i].Length % 2 == 0) {//even # of characters
					extraOffset = tempCharacterOffset.x / 2.0f;
				} else {//odd # of characterS
					extraOffset = 0;
				}

				xPos = transform.position.x - Mathf.FloorToInt(lines [i].Length  / 2.0f ) * tempCharacterOffset.x + (j * tempCharacterOffset.x) + extraOffset;

				GameObject shapePrefab = null; 
				bool shapeExits = shapesMapping.TryGetValue (lines [i] [j].ToString (), out shapePrefab);
				if (shapeExits) {
					CreateNewShape (shapePrefab, new Vector3 (xPos, yPos,0));
				}
			}
		}

	}


	private void CreateNewShape (GameObject prefab, Vector3 pos)
	{
		if (prefab == null) {
			return;
		}
	
		GameObject shapeGO = Instantiate (prefab, Vector3.zero, Quaternion.identity) as GameObject;
		shapeGO.transform.SetParent (transform);
		shapeGO.name = prefab.name;
		shapeGO.transform.position = pos;
		shapeGO.transform.localScale = prefab.transform.localScale;
		shapes.Add (shapeGO.GetComponent<Shape> ());
	}

	#endif
}
