﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardFlip : MonoBehaviour
{
    [SerializeField] private LayoutBoard boardManager;
    [SerializeField] private GameObject Card_Back;
    [SerializeField] private int _id;

    public int GetId()
    {
        return _id;
    }
    public int SetId(int id)
    {
       return _id = id;
    }
    public void ChangeSprite(int id, Sprite image)
    {
        CardFlip card = GetComponent<CardFlip>();
        card.SetId(id);
        GetComponent<Image>().sprite = image;
    }

    public void ShowCard()
    {
        if (Card_Back.activeSelf && boardManager.canShow) //check game object is active
        {
            Card_Back.SetActive(false);
            boardManager.CardShowed(this);
        }
    }
    public void HideCard()
    {
        if (!Card_Back.activeSelf) //check game object is active
        {
            Card_Back.SetActive(true);
        }
    }
}
