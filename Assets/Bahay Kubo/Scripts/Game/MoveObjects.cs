﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjects : MonoBehaviour {
    float deltaX;
    float deltaY;

    Rigidbody2D rb;

    bool moveAllowed = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0)
        {   //check touch
            Touch touch = Input.GetTouch(0);
            //get touch pos
            Vector2 touchPos = Camera.main.ScreenToWorldPoint(touch.position);
            //touch phase
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    {

                        if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
                        {
                            deltaX = touchPos.x - transform.position.x;
                            deltaY = touchPos.y - transform.position.y;

                            moveAllowed = true;

                            rb.freezeRotation = true;
                            rb.velocity = new Vector2(0, 0);
                            rb.gravityScale = 0;
                            GetComponent<BoxCollider2D>().sharedMaterial = null;
                        }
                        break;
                    }
                case TouchPhase.Moved:
                    {
                        if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos) && moveAllowed)
                        {
                            rb.MovePosition(new Vector2(touchPos.x - deltaX, touchPos.y - deltaY));
                        }
                        break;
                    }
                case TouchPhase.Ended:
                    {
                        moveAllowed = false;
                        rb.freezeRotation = false;
                        rb.gravityScale = 2;
                        PhysicsMaterial2D mat = new PhysicsMaterial2D();
                        mat.bounciness = 0.75f;
                        mat.friction = 0.4f;
                        GetComponent<BoxCollider2D>().sharedMaterial = mat;
                        break;
                    }

            }

        }
	}
}
