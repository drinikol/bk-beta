﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[DisallowMultipleComponent]
public class MusicToggle : MonoBehaviour
{
	public Sprite musicOn, musicOff;
	public Image musicButton;

	// Use this for initialization
	void Start ()
	{
		SetImageStatus ();
	}

	public void ToggleMusic ()
	{
		if (AudioSources.instance.audioSources [0].mute) {
			AudioSources.instance.UnMuteMusic ();
		} else {
			AudioSources.instance.MuteMusic ();
		}

		SetImageStatus ();
	}

	private void SetImageStatus(){

		if (AudioSources.instance.audioSources [0].mute) {
			musicButton.sprite = musicOff;
		} else {
			musicButton.sprite = musicOn;
		}
	}
}
