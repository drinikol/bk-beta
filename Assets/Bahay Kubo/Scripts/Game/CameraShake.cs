﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MEC;

public class CameraShake : MonoBehaviour
{
    // public static CameraShake instance;

    private Vector3 _originalPos;
    private float _timeAtCurrentFrame;
    private float _timeAtLastFrame;
    private float _fakeDelta;

    void Awake()
    {
        //  instance = this;
    }

    void Update()
    {
        _timeAtCurrentFrame = Time.realtimeSinceStartup;
        _fakeDelta = _timeAtCurrentFrame - _timeAtLastFrame;
        _timeAtLastFrame = _timeAtCurrentFrame;
    }

    public void ShakeCamera(float duration, float amount)
    {
        _originalPos = gameObject.transform.localPosition;

        Timing.KillCoroutines();
        Timing.RunCoroutine(_cShake(duration, amount));
    }


    public IEnumerator<float> _cShake(float duration, float amount)
    {
        // float endTime = Time.time + duration;

        while (duration > 0)
        {
            transform.localPosition = _originalPos + Random.insideUnitSphere * amount;

            duration -= _fakeDelta;

            yield return Timing.WaitForOneFrame;
        }

        transform.localPosition = _originalPos;
    }
    public IEnumerator<float> _cCanvas(float duration, float amount)
    {
        yield return 0;

    }
}