﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MEC;

[DisallowMultipleComponent]
public class UIEvents : MonoBehaviour
{
	/// <summary>
	/// Static instance of this class.
	/// </summary>
	public static UIEvents instance;
    public static string isWhatShape;
	void Awake(){
		if (instance == null) {
			instance = this;
		}
	}

	public void AlbumShapeEvent (TableShape tableShape)
	{
		if (tableShape == null) {
			return;
		}

		ShapesManager shapesManager = null;
		if (!string.IsNullOrEmpty (ShapesManager.shapesManagerReference)) {
			shapesManager = ShapesManager.shapesManagers[ShapesManager.shapesManagerReference];
		}

		if (shapesManager == null) {
			return;
		}

		if (shapesManager.shapes[tableShape.ID].isLocked) {
			return;
		}

		ShapesManager.Shape.selectedShapeID = tableShape.ID;
		LoadGameScene ();
	}

	public void PointerButtonEvent (Pointer pointer)
	{
		if (pointer == null) {
			return;
		}
		if (pointer.group != null) {
			ScrollSlider scrollSlider = GameObject.FindObjectOfType (typeof(ScrollSlider)) as ScrollSlider;
			if (scrollSlider != null) {
				scrollSlider.DisableCurrentPointer ();
				FindObjectOfType<ScrollSlider> ().currentGroupIndex = pointer.group.Index;
				scrollSlider.GoToCurrentGroup ();
			}
		}
	}

	public void LoadMainScene ()
	{
        //StartCoroutine(SceneLoader.LoadSceneAsync ("Main"));
        Timing.RunCoroutine(SceneLoader.LoadSceneAsync("Main"));
    }

	public void LoadGameScene ()
	{
        //StartCoroutine(SceneLoader.LoadSceneAsync ("Game"));
        Timing.RunCoroutine(SceneLoader.LoadSceneAsync("Game"));
    }

    public void LoadInfoScene()
    {
        //StartCoroutine(SceneLoader.LoadSceneAsync("Info"));
        Timing.RunCoroutine(SceneLoader.LoadSceneAsync("Info"));
    }

    public void LoadMatchGameScene()
    {
        //StartCoroutine(SceneLoader.LoadSceneAsync("Pronounce"));
        Timing.RunCoroutine(SceneLoader.LoadSceneAsync("matchGame"));
    }

    public void LoadPhonetixLetter()
    {
        string letter = EventSystem.current.currentSelectedGameObject.name;
        int letterInt = int.Parse(letter);

        GameObject phoeneticsHander = GameObject.FindGameObjectWithTag("PhonetixHandler");
        Timing.RunCoroutine(phoeneticsHander.GetComponent<PhoeneticsHandler>().StartPhonetix(letterInt).CancelWith(phoeneticsHander));

        TableShape loadLetter = GameObject.FindGameObjectWithTag("PhonetixTrace").GetComponent<TableShape>();
        loadLetter.SetID(letterInt);
    }

    public void LoadPhonetixNumber()
    {
        string letter = EventSystem.current.currentSelectedGameObject.name;
        int letterInt = int.Parse(letter);

        GameObject phoeneticsHander = GameObject.FindGameObjectWithTag("PhonetixHandler");
        Timing.RunCoroutine(phoeneticsHander.GetComponent<PhoeneticsHandler>().StartPhonetixNumbers(letterInt).CancelWith(phoeneticsHander));

        TableShape loadNumber = GameObject.FindGameObjectWithTag("PhonetixTrace").GetComponent<TableShape>();
        loadNumber.SetID(letterInt);
    }

    public void ShareLink()
    {
        NativeShare share = new NativeShare();
        share.SetSubject("Download the Bahay Kubo App!");
        share.SetTitle("Share the Bahay Kubo App");
        share.SetText(Links.appStore);
        share.Share();
    }

    public void RevealCard()
    {
        GameObject selected = EventSystem.current.currentSelectedGameObject;
        selected.GetComponent<CardFlip>().ShowCard();
    }

    public void ReviewApp()
    {
        Application.OpenURL(Links.appStore);
    }
    public void ResetAllData()
    {
        DataManager.ResetGame();
    }
    public void LoadAlbumScene ()
	{
		//Load Album scene based on current shapesManagerReference
		if (!string.IsNullOrEmpty (ShapesManager.shapesManagerReference)) {
			ShapesManager shapesManager = ShapesManager.shapesManagers[ShapesManager.shapesManagerReference];
			//StartCoroutine (SceneLoader.LoadSceneAsync (shapesManager.sceneName));
            Timing.RunCoroutine(SceneLoader.LoadSceneAsync(shapesManager.sceneName));
        }
	}

	public void LoadAlbumScene (string shapesManagerReference)
	{
		//Load Album scene based on given shapesManagerReference
		if (string.IsNullOrEmpty (shapesManagerReference)) {
			Debug.LogError ("Empty Shapes Manager Reference in the Button Component");
			return;
		}

        if(EventSystem.current.currentSelectedGameObject.name == "letters")
        {
            isWhatShape = "letters";
        }
        if (EventSystem.current.currentSelectedGameObject.name == "numbers")
        {
            isWhatShape = "numbers";
        }
        ShapesManager shapesManager = ShapesManager.shapesManagers[shapesManagerReference];
		ShapesManager.shapesManagerReference = shapesManagerReference;
		//StartCoroutine (SceneLoader.LoadSceneAsync (shapesManager.sceneName));
        Timing.RunCoroutine(SceneLoader.LoadSceneAsync(shapesManager.sceneName));
    }

	public void NextClickEvent ()
	{
		try {
			GameManager.instance.NextShape ();
		} catch (System.Exception ex) {

		}
	}

	public void PreviousClickEvent ()
	{
		try {
			GameManager.instance.PreviousShape ();
		} catch (System.Exception ex) {
			
		}
	}

	public void HelpClickEvent(){
		GameManager.instance.Help ();
	}

	public void SpeechClickEvent ()
	{
		GameManager.instance.Spell ();
	}

	public void ResetShape ()
	{
		if (GameManager.instance != null) {
			if (!GameManager.instance.shape.completed) {
				GameManager.instance.DisableGameManager ();
				GameObject.Find ("ResetConfirmDialog").GetComponent<Dialog> ().Show ();
			} else {
				GameManager.instance.ResetShape ();
			}
		}
	}

	public void PencilClickEvent (Pencil pencil)
	{
		if (pencil == null) {
			return;
		}
		if (GameManager.instance == null) {
			return;
		}
		if (GameManager.instance.currentPencil != null) {
			GameManager.instance.currentPencil.DisableSelection ();
			GameManager.instance.currentPencil = pencil;
		}
		GameManager.instance.SetShapeOrderColor ();
		pencil.EnableSelection ();
	}

	public void ResetConfirmDialogEvent (GameObject value)
	{
		if (value == null) {
			return;
		}
		
		if (value.name.Equals ("YesButton")) {
			Debug.Log ("Reset Confirm Dialog : Yes button clicked");
			if (GameManager.instance != null) {
				GameManager.instance.ResetShape ();
			}
			
		} else if (value.name.Equals ("NoButton")) {
			Debug.Log ("Reset Confirm Dialog : No button clicked");
		}

		value.GetComponentInParent<Dialog> ().Hide ();

		if (GameManager.instance != null) {
			GameManager.instance.EnableGameManager ();
		}
	}

	public void ResetGame ()
	{
		DataManager.ResetGame ();
	}

	public void LeaveApp ()
	{
		Application.Quit ();
	}
}
