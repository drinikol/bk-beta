﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[DisallowMultipleComponent]
public class ShapesManager : MonoBehaviour
{
	/// <summary>
	/// The shapes list.
	/// </summary>
	public List<Shape> shapes = new List<Shape> ();

	/// <summary>
	/// The collected stars in the shapes.
	/// </summary>
	private int collectedStars;

	/// <summary>
	/// The shape label (example Letter or Number).
	/// </summary>
	public string shapeLabel = "Shape";

	/// <summary>
	/// The shape prefix used for DataManager only (example Lowercase or Uppercase or Number).
	/// </summary>
	public string shapePrefix = "Shape";

	/// <summary>
	/// The name of the scene.
	/// </summary>
	public string sceneName = "Album";

	/// <summary>
	/// The title of the album scene.
	/// </summary>
	public Sprite albumTitle;

	/// <summary>
	/// The shape scale factor in the preview/album scene.
	/// </summary>
	public float albumShapeScaleFactor = 0.7f;


	/// <summary>
	/// The shape scale factor in the game scene.
	/// </summary>
	public float gameShapeScaleFactor = 1f;

	/// <summary>
	/// The last selected group.
	/// </summary>
	[HideInInspector]
	public int lastSelectedGroup;

	/// <summary>
	/// The name of the shapes manager.
	/// </summary>
	public static string shapesManagerReference = "";
		
	/// <summary>
	/// The list of the shapes managers in the scene.
	/// </summary>
	public static Dictionary<string, ShapesManager> shapesManagers = new Dictionary <string, ShapesManager> ();


	void Awake ()
	{
		if (shapesManagers.ContainsKey (gameObject.name)) {
			Destroy (gameObject);
		} else {
			//init values
			shapesManagers.Add (gameObject.name, this);

			for (int i = 0; i < shapes.Count; i++) {
				shapes [i].ID = i;
				shapes [i].isLocked = DataManager.IsShapeLocked (shapes [i].ID, this);
				shapes [i].starsNumber = DataManager.GetShapeStars (shapes [i].ID, this);

				if (shapes [i].ID == 0) {
					shapes [i].isLocked = false;
				}
			}

			collectedStars = CalculateCollectedStars ();
			DontDestroyOnLoad (gameObject);
			lastSelectedGroup = 0;
		}
	}

	void Start ()
	{

	}

	/// <summary>
	/// Calculate the collected stars in all shapes.
	/// </summary>
	/// <returns>The collected stars.</returns>
	private int CalculateCollectedStars(){
		int cs = 0;
		foreach (Shape shape in shapes) {
			if (shape.starsNumber == ShapesManager.Shape.StarsNumber.ONE) {
				cs+=1;
			} else if (shape.starsNumber == ShapesManager.Shape.StarsNumber.TWO) {
				cs+=2;
			} else if (shape.starsNumber == ShapesManager.Shape.StarsNumber.THREE) {
				cs+=3;
			}
		}
		return cs;
	}

	/// <summary>
	/// Get the stars rate of the shapes.
	/// </summary>
	/// <returns>The stars rate from 3.</returns>
	public int GetStarsRate(){
		return Mathf.FloorToInt(collectedStars /(shapes.Count * 3.0f) * 3.0f);
	}

	/// <summary>
	/// Get the collected stars.
	/// </summary>
	/// <returns>The collected stars.</returns>
	public int GetCollectedStars(){
		return collectedStars;
	}

	/// <summary>
	/// Set the collected stars.
	/// </summary>
	/// <param name="collectedStars">Collected stars.</param>
	public void SetCollectedStars(int collectedStars){
		this.collectedStars = collectedStars;
	}

	/// <summary>
	/// Get the current shape.
	/// </summary>
	/// <returns>The current shape.</returns>
	public Shape GetCurrentShape(){
		return shapes [Shape.selectedShapeID];
	}

	[System.Serializable]
	public class Shape
	{
		/// <summary>
		/// Whether to show the contents/shapes (used for Editor).
		/// </summary>
		public bool showContents = true;

		/// <summary>
		/// The game/shape prefab.
		/// </summary>
		public GameObject gamePrefab;

		/// <summary>
		/// The picture of the shape.
		/// </summary>
		public Sprite picture;

		/// <summary>
		/// The audio clip of the shape , used for spelling.
		/// </summary>
		public AudioClip clip;

		/// <summary>
		/// The stars time period.
		/// 0 - 14 seconds : 3 stars , 15 - 29 : 2 stars , otherwisee 1 star
		/// </summary>
		public int starsTimePeriod = 15;

		/// <summary>
		/// The number of the collected stars.
		/// </summary>
		[HideInInspector]
		public StarsNumber starsNumber = StarsNumber.ZERO;

		/// <summary>
		/// The ID of the shape.
		/// </summary>
		[HideInInspector]
		public int ID = -1;

		/// <summary>
		/// Whether the shape is locked or not.
		/// </summary>
		[HideInInspector]
		public bool isLocked = false; //default TRUE

		/// <summary>
		/// The ID selected/current shape.
		/// </summary>
		public static int selectedShapeID;

		public enum StarsNumber
		{
			ZERO,
			ONE,
			TWO,
			THREE
		}
	}
}
