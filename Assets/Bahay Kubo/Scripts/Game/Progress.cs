﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


[DisallowMultipleComponent]
public class Progress : MonoBehaviour
{
	/// <summary>
	/// The star off sprite.
	/// </summary>
	public Sprite starOff;
		
	/// <summary>
	/// The star on sprite.
	/// </summary>
	public Sprite starOn;

	/// <summary>
	/// The level stars.
	/// </summary>
	public Image[] levelStars;

	/// <summary>
	/// The progress image.
	/// </summary>
	public Image progressImage;

	/// <summary>
	/// The stars number.
	/// </summary>
	[HideInInspector]
	public ShapesManager.Shape.StarsNumber starsNumber;

	/// <summary>
	/// Static instance of this class.
	/// </summary>
	public static Progress instance;

	void Awake(){
		if (instance == null)
			instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		if (progressImage == null) {
			progressImage = GetComponent<Image> ();
		}
	}

	/// <summary>
	/// Set the value of the progress.
	/// </summary>
	/// <param name="currentTime">Current time.</param>
	public void SetProgress (float currentTime)
	{
		if (GameManager.instance == null) {
			return;
		}

		if (GameManager.instance.shape == null) {
			return;
		}

		if (progressImage != null)
			progressImage.fillAmount = 1 - (currentTime / (GameManager.instance.shapesManager.GetCurrentShape().starsTimePeriod * 3));
			
		if (currentTime >= 0 && currentTime <= GameManager.instance.shapesManager.GetCurrentShape().starsTimePeriod) {
			if (levelStars [0] != null) {
				levelStars [0].sprite = starOn;
			}
			if (levelStars [1] != null) {
				levelStars [1].sprite = starOn;
			}
			if (levelStars [2] != null) {
				levelStars [2].sprite = starOn;
			}
			if (progressImage != null)
				progressImage.color = Colors.greenColor;

			starsNumber = ShapesManager.Shape.StarsNumber.THREE;
		} else if (currentTime > GameManager.instance.shapesManager.GetCurrentShape().starsTimePeriod && currentTime <= 2 * GameManager.instance.shapesManager.GetCurrentShape().starsTimePeriod) {
			if (levelStars [2] != null) {
				levelStars [2].sprite = starOff;
			}
			if (progressImage != null)
				progressImage.color = Colors.yellowColor;
			starsNumber = ShapesManager.Shape.StarsNumber.TWO;

		} else {
			if (levelStars [1] != null) {
				levelStars [1].sprite = starOff;
			}
			if (levelStars [2] != null) {
				levelStars [2].sprite = starOff;
			}
			if (progressImage != null)
				progressImage.color = Colors.redColor;
			starsNumber = ShapesManager.Shape.StarsNumber.ONE;
		}
	}

}
