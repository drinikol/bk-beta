﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

[DisallowMultipleComponent]
public class AudioSources : MonoBehaviour {

	/// <summary>
	/// This Gameobject defined as a Singleton.
	/// </summary>
	public static AudioSources instance;

	/// <summary>
	/// The audio sources references.
	/// First Audio Souce used for the music
	/// Second Audio Souce used for the sound effects
	/// </summary>
	[HideInInspector]
	public AudioSource [] audioSources;

    /// <summary>
    /// The Correct sound effect.
    /// </summary>
    public AudioClip correctSFX;

    /// <summary>
    /// The Wrong sound effect.
    /// </summary>
    public AudioClip errorSFX;
    /// <summary>
    /// The bubble sound effect.
    /// </summary>
    /// 
    public AudioClip bubbleSFX;
    [SerializeField] AudioClip[] audioClips;
    /// <summary>
    /// The music status key.
    /// </summary>
    private const string musicStatusKey = "MusicStatus";

	// Use this for initialization
	void Awake ()
	{
		if (instance == null) {
			instance = this;
			audioSources = GetComponents<AudioSource>();

			if (PlayerPrefs.GetInt (musicStatusKey) == 0) {
				UnMuteMusic ();	
			} else {
				MuteMusic ();
			}
            Timing.RunCoroutine(PlayMusic());
			DontDestroyOnLoad(gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	void Start(){
	}

	/// <summary>
	/// Mute the music.
	/// </summary>
	public void MuteMusic(){
		audioSources [0].mute = true;
		SaveData ();
	}

	/// <summary>
	/// Unmute the music.
	/// </summary>
	public void UnMuteMusic(){
		audioSources [0].mute = false;
		SaveData ();
	}

	/// <summary>
	/// Save music mute status to playerpref
	/// </summary>
	private void SaveData(){
		if (audioSources [0].mute) {
			PlayerPrefs.SetInt (musicStatusKey, 1);
		} else {
			PlayerPrefs.SetInt (musicStatusKey, 0);
		}

		PlayerPrefs.Save ();
	}

	public void PlayBubbleSFX(){
		if (bubbleSFX != null && audioSources[1] != null) {
			CommonUtil.PlayOneShotClipAt (bubbleSFX, Vector3.zero, audioSources[1].volume);
		}

	}
    IEnumerator<float> PlayMusic()
    {
        audioSources[0].clip = audioClips[0];
        audioSources[0].Play();
        yield return Timing.WaitForSeconds(audioSources[0].clip.length);
        audioSources[0].clip = audioClips[1];
        audioSources[0].Play();
    }
    /// <summary>
    /// Play the correct SFX.
    /// </summary>
    public void PlayCorrectSFX()
    {
        if (correctSFX != null && audioSources[1] != null)
        {
            CommonUtil.PlayOneShotClipAt(correctSFX, Vector3.zero, audioSources[1].volume);
        }
    }
    /// <summary>
    /// Play the ERROR SFX.
    /// </summary>
    public void PlayErrorSFX()
    {
        if (errorSFX != null && audioSources[1] != null)
        {
            CommonUtil.PlayOneShotClipAt(errorSFX, Vector3.zero, audioSources[1].volume);
        }
    }
}
