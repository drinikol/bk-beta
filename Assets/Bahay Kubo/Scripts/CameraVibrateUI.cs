﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraVibrateUI : MonoBehaviour {

    public float shakeAmount = 0.7f;
    public Canvas canvas;
    float shakeTime = 0.0f;
    Vector3 initialPosition;
    public void VibrateForTime(float time)
    {
        shakeTime = time;

        canvas.renderMode = RenderMode.WorldSpace;
    }
    void Start()
    {
        initialPosition = this.transform.position;
    }
    void Update()
    {
        if (shakeTime > 0)
        {
            this.transform.position = Random.insideUnitSphere * shakeAmount + initialPosition;
            shakeTime -= Time.deltaTime;
        }
        else
        {
            shakeTime = 0.0f;
            this.transform.position = initialPosition;
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
        }
    }

}
