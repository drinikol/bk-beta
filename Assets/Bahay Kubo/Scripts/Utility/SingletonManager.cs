﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



[DisallowMultipleComponent]
public class SingletonManager : MonoBehaviour {

	public GameObject[] values;

	// Use this for initialization
	void Awake () {
		InitManagers ();
	}

	private void InitManagers ()
	{
		if (values == null) {
			return;
		}

		foreach (GameObject value in values) {
			if (GameObject.Find (value.name) == null) {
				GameObject temp = Instantiate (value, Vector3.zero, Quaternion.identity) as GameObject;
				temp.name = value.name;
			}
		}
	}

	public List<ShapesManager> GetShapesManagers(){
		List<ShapesManager> shapesManagers = new List<ShapesManager> ();

		foreach (GameObject value in values) {
			ShapesManager sm = value.GetComponent<ShapesManager> ();
			if (sm != null) {
				shapesManagers.Add (sm);
			}
		}
		return shapesManagers;
	}
}
